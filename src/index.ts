import { GraphQLServer } from 'graphql-yoga'
import {prisma, SituationCreateInput} from '../prisma/generated/prisma-client'
import { Context } from './utils'
import { getGraphQLProjectConfig, GraphQLProjectConfig } from 'graphql-config'
import { Situation } from './models/situation'

const resolvers = {
  Query: {
    situations(parent, args, context: Context) {
      return context.prisma.situations()
    }
  },
  Mutation: {
    createSituation(parent, situation: SituationCreateInput, context: Context) {
      return context.prisma.createSituation(situation)
    },
  },
}

const config: GraphQLProjectConfig = getGraphQLProjectConfig('../', 'auth')
const endpoint: string = config.extensions.endpoints.default as string
const port: string = config.extensions.port as string
const options = {
  port: port || '4001',
  endpoint: endpoint || '/graphql',
  subscription: '/subscription',
  playground: '/playground'
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers,
  context: { prisma },
})
server.start(options, () => console.log(`Server is running on http://localhost:${options.port}`))
