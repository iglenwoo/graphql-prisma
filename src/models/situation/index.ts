export enum SITUATION_TYPE {
    ACTIVE_SHOOTING
}

export enum SITUATION_STATE {
    VALID,
    EVACUATE,
    REPORT,
    CLOSED
}

export interface Situation {
    situation_id?: string,
    type: SITUATION_TYPE,
    state: SITUATION_STATE,
    latitude: string,
    longitude: string,
    radius: number,
    location: string,
    keywords: string[],
    title: string,
    description?: string,
    started_at: string,
    ended_at?: string,
    approved_by: string,
    last_modified_by: string,
    last_modified_at?: string,
}